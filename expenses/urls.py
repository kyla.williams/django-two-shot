from django.contrib import admin
from django.urls import include, path
from django.shortcuts import redirect


def redirect_to_receipts(request):
    return redirect("receipts:home")


# Define the URL patterns
urlpatterns = [
    path("admin/", admin.site.urls),
    path("receipts/", include("receipts.urls")),
    path("", redirect_to_receipts),
    path("accounts/", include("accounts.urls")),
]
