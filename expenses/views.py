from django.shortcuts import render


def index(request):
    return render(request, "expenses/index.html")


def about(request):
    return render(request, "expenses/about.html")
