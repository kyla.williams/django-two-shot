from django.urls import path
from .views import receipt_list
from . import views

app_name = "receipts"

urlpatterns = [
    path("accounts/create/", views.create_account, name="create_account"),
    path("categories/create/", views.create_category, name="create_category"),
    path("categories/", views.category_list, name="category_list"),
    path("accounts/", views.account_list, name="account_list"),
    path("", views.receipt_list, name="home"),
    path("create/", views.create_receipt, name="create_receipt"),
]
