from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from receipts.models import ExpenseCategory, Account, Receipt
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm

# Create your views here.


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(
        request, "receipts/receipt_list.html", {"receipts": receipts}
    )


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = (
                request.user
            )  # Set the purchaser to the current logged-in user
            receipt.save()
            return redirect(
                "receipts:home"
            )  # Redirect to the home page (change 'home' to your actual home URL name)
    else:
        form = ReceiptForm()

    return render(request, "receipts/create_receipt.html", {"form": form})


@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    category_data = []

    for category in categories:
        receipt_count = Receipt.objects.filter(category=category).count()
        category_data.append(
            {"name": category.name, "receipt_count": receipt_count}
        )

    return render(
        request, "receipts/category_list.html", {"categories": category_data}
    )


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    account_data = []

    for account in accounts:
        receipt_count = Receipt.objects.filter(account=account).count()
        account_data.append(
            {
                "name": account.name,
                "number": account.number,
                "receipt_count": receipt_count,
            }
        )

    return render(
        request, "receipts/account_list.html", {"accounts": account_data}
    )


@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect("receipts:category_list")
    else:
        form = ExpenseCategoryForm()

    return render(request, "receipts/create_category.html", {"form": form})

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect('receipts:account_list')
    else:
        form = AccountForm()

    return render(request, 'receipts/create_account.html', {'form': form})
