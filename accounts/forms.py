from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm


class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)


class SignUpForm(UserCreationForm):
    password_confirmation = forms.CharField(
        max_length=150, widget=forms.PasswordInput
    )

    class Meta:
        model = User
        fields = [
            "username",
            "password1",
            "password2",
            "password_confirmation",
        ]

    def clean(self):
        cleaned_data = super().clean()
        password1 = cleaned_data.get("password1")
        password2 = cleaned_data.get("password2")
        password_confirmation = cleaned_data.get("password_confirmation")

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("The passwords do not match.")
